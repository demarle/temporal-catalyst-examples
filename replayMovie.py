# A sample Catalyst Python pipeline with an ex post facto trigger.
# Here we wait until particles collide and then once they do
# we find out which particles hit and render their paths up to the
# point of collision.

from paraview.simple import *
from paraview import coprocessing

#--------------------------------------------------------------
# Code generated from cpstate.py to create the CoProcessor.


# ----------------------- CoProcessor definition -----------------------

def CreateCoProcessor():
  def _CreatePipeline(coprocessor, datadescription):
    class Pipeline:
      # KEY POINT:
      # We don't have to do anything special at each timestep.
      # But we do have to make the data available.
      # In this case the recent data.
      data = coprocessor.CreateTemporalProducer( datadescription, "volume" )
      volDisplay = Show(data)
      volDisplay.SetRepresentationType('Outline')

      #make up controllable geometry for the six sides
      bounds = data.GetDataInformation().GetBounds()
      print(bounds)
      planes = []
      planeReps = []
      for s in range(0,6):
        planes.append(Plane())
        pr = Show()
        planeReps.append(pr)
        planeReps[s].Opacity = 0.4
        planeReps[s].DiffuseColor = [0.1,0.1,1.0]
        planeReps[s].Visibility = 0
      planes[0].Origin = [bounds[0], bounds[2], bounds[4]] #left
      planes[0].Point1 = [bounds[0], bounds[3], bounds[4]]
      planes[0].Point2 = [bounds[0], bounds[2], bounds[5]]
      planes[1].Origin = [bounds[0], bounds[2], bounds[5]] #front
      planes[1].Point1 = [bounds[0], bounds[3], bounds[5]]
      planes[1].Point2 = [bounds[1], bounds[2], bounds[5]]
      planes[2].Origin = [bounds[1], bounds[2], bounds[5]] #right
      planes[2].Point1 = [bounds[1], bounds[3], bounds[5]]
      planes[2].Point2 = [bounds[1], bounds[2], bounds[4]]
      planes[3].Origin = [bounds[1], bounds[2], bounds[4]] #back
      planes[3].Point1 = [bounds[1], bounds[3], bounds[4]]
      planes[3].Point2 = [bounds[0], bounds[2], bounds[4]]
      planes[4].Origin = [bounds[0], bounds[2], bounds[4]] #bottom
      planes[4].Point1 = [bounds[0], bounds[2], bounds[5]]
      planes[4].Point2 = [bounds[1], bounds[2], bounds[4]]
      planes[5].Origin = [bounds[1], bounds[3], bounds[4]] #top
      planes[5].Point1 = [bounds[1], bounds[3], bounds[5]]
      planes[5].Point2 = [bounds[0], bounds[3], bounds[4]]

      # a threshold filter to extract collisions from
      threshold1 = Threshold(Input=data)
      threshold1.Scalars = ['CELLS', 'occupancy']
      threshold1.ThresholdRange = [2.0, 10000.0]      

      points = coprocessor.CreateTemporalProducer( datadescription, "points" )
      #points = coprocessor.CreateProducer( datadescription, "points" )

    return Pipeline()

  class CoProcessor(coprocessing.CoProcessor):
    first = True
    def CreatePipeline(self, datadescription):
      self.Pipeline = _CreatePipeline(self, datadescription)
      # some bookkeeping structures to make a nice set of outputs
      self.StepsWritten = set()
      self.TimesToTimesteps = {}
      self.CCounter = 0
      self.CollisionWeights = {}
      self.LastCollisionWeights = {}

    def ProcessTriggers(self, datadescription):
      # In this trigger we Look at the simulation's output, and see if we need to do anything
      # out of the ordinary. We have access to the N most recent outputs so we write them all out to disk.
      data = self.Pipeline.data
      data.UpdatePipelineInformation()
      dtimes = data.TimestepValues
      timestep = datadescription.GetTimeStep()
      time = datadescription.GetTime()
      self.TimesToTimesteps[time] = timestep
      data.UpdatePipeline(time)

      points = self.Pipeline.points
      points.UpdatePipeline(time)

      writer = servermanager.writers.XMLPUnstructuredGridWriter(Input=points)
      writer.FileName =  "t_%d.pvtu" % timestep
      #writer.UpdatePipeline(time)
            
      #orange[1] > 1 indicates a collision
      orange = data.GetDataInformation().GetCellDataInformation().GetArrayInformation("occupancy").GetComponentRange(0)

      # must agree in parallel
      from mpi4py import MPI
      comm = MPI.COMM_WORLD
      rank = comm.Get_rank()
      lOccupancy = orange[1]
      gOccupancy = comm.gather(orange[1])
      anyHits = False
      if rank == 0:
        for res in gOccupancy:
          if res > 1:
            anyHits = True
      anyHits = comm.bcast(anyHits)
      #print ("%d %d %d %d" % (rank, timestep, lOccupancy, anyHits))

      localPointProducer = points.GetClientSideObject()
      localPoints = localPointProducer.GetOutputDataObject(0)
      NP = localPoints.GetNumberOfPoints()
      self.CollisionWeights = {}
      if anyHits:
        # a threshold to find out where the collision(s) occured
        threshold1 = FindSource('Threshold1')
        threshold1.UpdatePipeline(time)
        localThreshFilter = threshold1.GetClientSideObject()
        cells = localThreshFilter.GetOutput()
        ncells = cells.GetNumberOfCells()
        centroid = [0,0,0]
        occupancyArray = cells.GetCellData().GetArray("occupancy")

        # a point locator on the input points to find out who was involved
        idArray = localPoints.GetPointData().GetArray('pointid')
        rArray = localPoints.GetPointData().GetArray('radius')
        import vtk
        pl = vtk.vtkPointLocator()
        pl.SetDataSet(localPoints)
        pl.BuildLocator()
        rl = vtk.vtkIdList()

        # at every cell where a collision happened (of N particles)
        # the N nearest particles were responsible
        newColliders = set()
        for x in range(0, ncells):
          num = occupancyArray.GetValue(x)
          cells.GetCell(x).GetCentroid(centroid)
          #print(centroid, num)
          pl.FindClosestNPoints(int(num), centroid, rl)
          for p in range(0, rl.GetNumberOfIds()):
            #print(rl.GetId(p), idArray.GetValue(rl.GetId(p)), rArray.GetValue(rl.GetId(p)))
            newColliders.add(idArray.GetValue(rl.GetId(p)))

        # must agree in parallel
        #print("L newColliders are: ", sorted(newColliders))
        gColliders = comm.gather(newColliders)
        if rank == 0:
          for hits in gColliders:
            newColliders.update(hits)
        newColliders = comm.bcast(newColliders)
        #print("G newColliders are: ", sorted(newColliders))
        # a collision!
        print ("collision at tstep %d, time %f, maxoccupancy %f" % (timestep, time, orange[1]))
        print ("NEWCOLLIDERS", newColliders)

        for hit in newColliders:          
          self.CollisionWeights[hit] = 1.0
      else:
        print ("no collision tstep %d" % timestep)

      #save image of current timestep
      # a glyph filter to show all the particles
      #TODO: glyph filter is crappy from python get rid of this
      glyph1 = Glyph(registrationName='Glyph1', Input=points, GlyphType='Sphere')
      glyph1.UpdatePipeline(time)
      glyph1.ScaleArray = ['POINTS', 'radius']
      glyph1.ScaleFactor = 2.0
      glyph1.GlyphMode = 'All Points'
      glyph1Display = Show(glyph1, GetActiveView(), 'GeometryRepresentation')
      glyph1Display.Representation = 'Surface'
      ColorBy(glyph1Display, ('POINTS', 'pointid'))

      # use color to emphasize the ones involved
      pointidLUT = GetColorTransferFunction('pointid')
      pointidLUT.EnableOpacityMapping = 1
      pointidPWF = GetOpacityTransferFunction('pointid')
      pointidLUT.ScalarRangeInitialized = 1
      pointidPWF.ScalarRangeInitialized = 1
      # a step function turned on for each collider, off for everyone else
      deemph = 0.1
      emph = 1.0
      opLUF = [-1, deemph,deemph,deemph]
      opLUF.extend([-0.75, deemph,deemph,deemph])
      for x in sorted(self.CollisionWeights):
        opLUF.extend([x-0.45, deemph,deemph,deemph])
        opLUF.extend([x-0.25, emph,emph,emph])
        opLUF.extend([x+0.25, emph,emph,emph])
        opLUF.extend([x+0.45, deemph,deemph,deemph])
      opLUF.extend([NP, deemph,deemph,deemph])
      #pointidLUT.RGBPoints = opLUF
      pointidPWF.Points = opLUF

      # write the current frame with the collision
      myView = GetActiveView()
      myView.ViewTime = time;
      if self.first:
        ResetCamera(myView)

        myView.EnableRayTracing = 0
        #materialLibrary1 = GetMaterialLibrary()
        myView.BackEnd = 'OSPRay pathtracer'
        myView.Background = [0.0, 0.0, 0.0]
        myView.EnvironmentalBG = [0.0, 0.0, 0.0]
        myView.SamplesPerPixel = 10
        c = GetActiveCamera()
        c.Azimuth(60)

        # annotate time
        annotateTime1 = AnnotateTime(registrationName='AnnotateTime1', Format='t=%.2f')
        #TODO: newly incompatible with OSP2 apparently
        annotateTime1Display = Show(annotateTime1, myView, 'TextSourceRepresentation')
        self.first = False
        self.writeCount = 0
      print ("frame_%d.png" % self.writeCount)

      annotateTime1 = FindSource('AnnotateTime1')
      if anyHits:
        annotateTime1.Format = 'HIT AT t=%.2f'
        #TODO: use luminous material on the colliders for emphasis
      else:
        annotateTime1.Format = 't=%.2f'
      SaveScreenshot("frame_%d.png"%self.writeCount, myView, ImageResolution=[1024,768])
      self.writeCount = self.writeCount + 1

      # TODO: record clocktime with each screenshot so we can restore faithfully
      # but without screen capture
      if anyHits and self.CollisionWeights != self.LastCollisionWeights:

        for r in range(0,2):
          SaveScreenshot("frame_%d.png"%self.writeCount, myView, ImageResolution=[1024,768])
          self.writeCount = self.writeCount + 1

        emph = 0.75
        opLUF = [-1, deemph,deemph,deemph]
        opLUF.extend([-0.75, deemph,deemph,deemph])
        for x in sorted(self.CollisionWeights):
          opLUF.extend([x-0.45, deemph,deemph,deemph])
          opLUF.extend([x-0.25, emph,emph,emph])
          opLUF.extend([x+0.25, emph,emph,emph])
          opLUF.extend([x+0.45, deemph,deemph,deemph])
        opLUF.extend([NP, deemph,deemph,deemph])
        pointidPWF.Points = opLUF

        for r in range(0,1):
          annotateTime1.Format = 'rewind...'
          rev = list(reversed(dtimes))
          for t in rev[::3]:
            tstep = self.TimesToTimesteps[t]
            myView.ViewTime = t;
            print ("backtrack frame_%d.png" % self.writeCount)
            SaveScreenshot("frame_%d.png"%self.writeCount, myView, ImageResolution=[1024,768])
            self.writeCount = self.writeCount + 1

          annotateTime1.Format = 'INSTANT REPLAY!!!!'
          for t in dtimes:
            for s in range(0,6):
               self.Pipeline.planeReps[s].Visibility = 0
            tstep = self.TimesToTimesteps[t]

            points.UpdatePipeline(t)
            localPoints = localPointProducer.GetOutputDataObject(0)
            posArray = localPoints.GetPoints()
            for hitter in self.CollisionWeights:
              loc = posArray.GetPoint(hitter)
              print(hitter, loc)
              if (abs(loc[0] - self.Pipeline.bounds[0]) < 10):
                print("left")
                self.Pipeline.planeReps[0].Visibility = 1
              if (abs(loc[0] - self.Pipeline.bounds[1]) < 10):
                print("right")
                self.Pipeline.planeReps[2].Visibility = 1
              if (abs(loc[1] - self.Pipeline.bounds[2]) < 10):
                print("bottom")
                self.Pipeline.planeReps[4].Visibility = 1
              if (abs(loc[1] - self.Pipeline.bounds[3]) < 10):
                print("top")
                self.Pipeline.planeReps[5].Visibility = 1
              if (abs(loc[2] - self.Pipeline.bounds[4]) < 10):
                print("back")
                self.Pipeline.planeReps[3].Visibility = 1
              if (abs(loc[2] - self.Pipeline.bounds[5]) < 10):
                print("front")
                self.Pipeline.planeReps[1].Visibility = 1

            myView.ViewTime = t;
            print ("replay frame_%d.png" % self.writeCount)
            SaveScreenshot("frame_%d.png"%self.writeCount, myView, ImageResolution=[1024,768])
            self.writeCount = self.writeCount + 1
          annotateTime1.Format = 'now in slow motion.'

          for t in dtimes:
            for s in range(0,6):
               self.Pipeline.planeReps[s].Visibility = 0
            tstep = self.TimesToTimesteps[t]

            points.UpdatePipeline(t)
            localPoints = localPointProducer.GetOutputDataObject(0)
            posArray = localPoints.GetPoints()
            for hitter in self.CollisionWeights:
              loc = posArray.GetPoint(hitter)
              print(hitter, loc)
              if (abs(loc[0] - self.Pipeline.bounds[0]) < 10):
                print("left")
                self.Pipeline.planeReps[0].Visibility = 1
              if (abs(loc[0] - self.Pipeline.bounds[1]) < 10):
                print("right")
                self.Pipeline.planeReps[2].Visibility = 1
              if (abs(loc[1] - self.Pipeline.bounds[2]) < 10):
                print("bottom")
                self.Pipeline.planeReps[4].Visibility = 1
              if (abs(loc[1] - self.Pipeline.bounds[3]) < 10):
                print("top")
                self.Pipeline.planeReps[5].Visibility = 1
              if (abs(loc[2] - self.Pipeline.bounds[4]) < 10):
                print("back")
                self.Pipeline.planeReps[3].Visibility = 1
              if (abs(loc[2] - self.Pipeline.bounds[5]) < 10):
                print("front")
                self.Pipeline.planeReps[1].Visibility = 1

            myView.ViewTime = t;
            for slowmow in range(0,3):
              print ("slowmo frame_%d.png" % self.writeCount)
              SaveScreenshot("frame_%d.png"%self.writeCount, myView, ImageResolution=[1024,768])
              self.writeCount = self.writeCount + 1

          for s in range(0,6):
            self.Pipeline.planeReps[s].Visibility = 0

      # prevents doing replay on every frame of multi frame collision
      self.LastCollisionWeights = self.CollisionWeights
      Hide(glyph1) # bug, I am adding to pipeline every frame, TODO: don't

  coprocessor = CoProcessor()
  freqs = {'volume': [10, 100]}
  coprocessor.SetUpdateFrequencies(freqs)
  return coprocessor

#--------------------------------------------------------------
# Global variables that will hold the pipeline for each timestep
# Creating the CoProcessor object, doesn't actually create the ParaView pipeline.
# It will be automatically setup when coprocessor.UpdateProducers() is called the
# first time.
coprocessor = CreateCoProcessor()

#--------------------------------------------------------------
# Enable Live-Visualizaton with ParaView
coprocessor.EnableLiveVisualization(True, 1)


# ---------------------- Data Selection method ----------------------

def RequestDataDescription(datadescription):
    "Callback to populate the request for current timestep"
    global coprocessor
    if datadescription.GetForceOutput() == True:
        # We are just going to request all fields and meshes from the simulation
        # code/adaptor.
        for i in range(datadescription.GetNumberOfInputDescriptions()):
            datadescription.GetInputDescription(i).AllFieldsOn()
            datadescription.GetInputDescription(i).GenerateMeshOn()
        return

    # setup requests for all inputs based on the requirements of the
    # pipeline.
    coprocessor.LoadRequestedData(datadescription)

# ------------------------ Processing method ------------------------

def DoCoProcessing(datadescription):
    "Callback to do co-processing for current timestep"
    global coprocessor

    # Update the coprocessor by providing it the newly generated simulation data.
    # If the pipeline hasn't been setup yet, this will setup the pipeline.
    coprocessor.UpdateProducers(datadescription)

    # KEY POINT:
    # We make up a trigger here that is evaluated on every timestep.
    coprocessor.ProcessTriggers(datadescription)

    # Write output data, if appropriate.
    coprocessor.WriteData(datadescription);

    # Write image capture (Last arg: rescale lookup table), if appropriate.
    coprocessor.WriteImages(datadescription, rescale_lookuptable=False)

    # Live Visualization, if enabled.
    coprocessor.DoLiveVisualization(datadescription, "localhost", 22222)
