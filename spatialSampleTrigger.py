#--------------------------------------------------------------

# Global timestep output options
timeStepToStartOutputAt=0
forceOutputAtFirstCall=False

# Global screenshot output options
imageFileNamePadding=0
rescale_lookuptable=False

# Whether or not to request specific arrays from the adaptor.
requestSpecificArrays=False

# a root directory under which all Catalyst output goes
rootDirectory=''

# makes a cinema D index table
make_cinema_table=False

#--------------------------------------------------------------
# Code generated from cpstate.py to create the CoProcessor.
# paraview version 5.8.0
#--------------------------------------------------------------

from paraview.simple import *
from paraview import coprocessing

# ----------------------- CoProcessor definition -----------------------

def CreateCoProcessor():
  def _CreatePipeline(coprocessor, datadescription):
    class Pipeline:
      # state file generated using paraview version 5.8.0

      # ----------------------------------------------------------------
      # setup views used in the visualization
      # ----------------------------------------------------------------

      # trace generated using paraview version 5.8.0
      #
      # To ensure correct image size when batch processing, please search
      # for and uncomment the line `# renderView*.ViewSize = [*,*]`

      #### disable automatic camera reset on 'Show'
      paraview.simple._DisableFirstRenderCameraReset()

      # Create a new 'Render View'
      renderView1 = CreateView('RenderView')
      renderView1.ViewSize = [1446, 916]
      renderView1.CenterOfRotation = [199.5, 50.0, 150.0]
      renderView1.CameraPosition = [666.2595792155953, -781.8060598841134, 409.0081655254331]
      renderView1.CameraFocalPoint = [287.3721961441632, 90.98181325575052, 159.9359253581938]
      renderView1.CameraViewUp = [-0.09121626726879445, 0.23645994531973918, 0.9673501366335429]
      renderView1.CameraParallelScale = 254.55893227305933

      # register the view with coprocessor
      # and provide it with information such as the filename to use,
      # how frequently to write the images, etc.
      """
      # if dumping normal data, catalyst ignores the time and 
      # we end up with unchanging images. So comment out here for now.
      coprocessor.RegisterView(renderView1,
          filename='RenderView1_%t.png', freq=1, fittoscreen=0, magnification=1, width=1446, height=916, cinema={}, compression=5)
      renderView1.ViewTime = datadescription.GetTime()
      """
      SetActiveView(None)

      # ----------------------------------------------------------------
      # setup view layouts
      # ----------------------------------------------------------------

      # create new layout object 'Layout #1'
      layout1 = CreateLayout(name='Layout #1')
      layout1.AssignView(0, renderView1)

      # ----------------------------------------------------------------
      # restore active view
      SetActiveView(renderView1)
      # ----------------------------------------------------------------

      # ----------------------------------------------------------------
      # setup the data processing pipelines
      # ----------------------------------------------------------------

      # create a new 'XML Partitioned Rectilinear Grid Reader'
      # create a producer from a simulation input
      data = coprocessor.CreateTemporalProducer(datadescription, 'volume')

      # create a new 'Threshold'
      threshold1 = Threshold(Input=data)
      threshold1.Scalars = ['CELLS', 'occupancy']
      threshold1.ThresholdRange = [1.0, 3.0]

      # create a new 'Point Source'
      pointSource1 = PointSource()
      pointSource1.Center = [35.0, 33.0, 28.0]

      # create a new 'Resample With Dataset'
      resampleWithDataset1 = ResampleWithDataset(SourceDataArrays=data,
          DestinationMesh=pointSource1)
      resampleWithDataset1.CellLocator = 'Static Cell Locator'

      # ----------------------------------------------------------------
      # setup the visualization in view 'renderView1'
      # ----------------------------------------------------------------

      # show data from threshold1
      threshold1Display = Show(threshold1, renderView1, 'UnstructuredGridRepresentation')

      # trace defaults for the display properties.
      threshold1Display.Representation = 'Surface'


      # show data from resampleWithDataset1
      resampleWithDataset1Display = Show(resampleWithDataset1, renderView1, 'GeometryRepresentation')

      # trace defaults for the display properties.
      resampleWithDataset1Display.Representation = 'Surface'

      # ----------------------------------------------------------------
      # finally, restore active source
      SetActiveSource(data)
      # ----------------------------------------------------------------
    return Pipeline()

  class CoProcessor(coprocessing.CoProcessor):
    def CreatePipeline(self, datadescription):
      self.Pipeline = _CreatePipeline(self, datadescription)
      self.ContinueFor = 2

      # some bookkeeping structures
      self.StepsWritten = set()
      self.TimesToTimesteps = {}
      self.CCount = 0
      self.CamReset = False

    def ProcessTriggers(self, datadescription):
      data = self.Pipeline.data
      timestep = datadescription.GetTimeStep()
      time = datadescription.GetTime()
      self.TimesToTimesteps[time] = timestep
      data.UpdatePipeline(time)

      # get the output of the resampleWithDataset1 to use as trigger input
      resampleWithDataset1 = FindSource('ResampleWithDataset1')
      resampleWithDataset1.UpdatePipeline(time)
      import vtk
      controller = vtk.vtkMultiProcessController.GetGlobalController()
      pressureD = [1.]
      if controller.GetLocalProcessId() == 0:
        result = servermanager.Fetch(resampleWithDataset1,0)
        pressureD = [result.GetPointData().GetArray('occupancy').GetValue(0)]
        print("sample is ", pressureD)
      controller.Broadcast(pressureD, 1, 0) # assumes resample collects to root. That seems to be true.

      if pressureD[0] > 0.0:
        self.CCount = self.ContinueFor
        print("a hit") #you sunk my battleship?

      if self.CCount > 1:
        self.CCount = self.CCount - 1
        data.UpdatePipelineInformation()
        dtimes = data.TimestepValues
        for t in dtimes:
            if (t not in self.StepsWritten):
                print ("save screenshot ", t)
                self.StepsWritten.add(t)
                tstep = self.TimesToTimesteps[t]

                # save out data directly
                #writer = servermanager.writers.XMLPImageDataWriter(Input=data)
                #writer.FileName =  "tevent_%d.pvti" % tstep
                #writer.UpdatePipeline(t)

                #or

                # save a screen shot at this timestep
                myView = GetActiveView()
                myView.ViewTime = t
                if not self.CamReset:
                    #hacky way to prevent zooming in/out at bbox changes
                    ResetCamera(myView)
                    self.CamReset = True
                SaveScreenshot("frame_%d.png"%tstep, myView, ImageResolution=[1446, 916])



  coprocessor = CoProcessor()
  # these are the frequencies at which the coprocessor updates.
  freqs = {'volume': [1]}
  coprocessor.SetUpdateFrequencies(freqs)

  return coprocessor


#--------------------------------------------------------------
# Global variable that will hold the pipeline for each timestep
# Creating the CoProcessor object, doesn't actually create the ParaView pipeline.
# It will be automatically setup when coprocessor.UpdateProducers() is called the
# first time.
coprocessor = CreateCoProcessor()

#--------------------------------------------------------------
# Enable Live-Visualizaton with ParaView and the update frequency
coprocessor.EnableLiveVisualization(False, 1)

# ---------------------- Data Selection method ----------------------

def RequestDataDescription(datadescription):
    "Callback to populate the request for current timestep"
    global coprocessor

    # setup requests for all inputs based on the requirements of the
    # pipeline.
    coprocessor.LoadRequestedData(datadescription)

# ------------------------ Processing method ------------------------

def DoCoProcessing(datadescription):
    "Callback to do co-processing for current timestep"
    global coprocessor

    # Update the coprocessor by providing it the newly generated simulation data.
    # If the pipeline hasn't been setup yet, this will setup the pipeline.
    coprocessor.UpdateProducers(datadescription)

    # We make up a trigger here that is evaluated on every timestep.
    coprocessor.ProcessTriggers(datadescription)

    # Write output data, if appropriate.
    coprocessor.WriteData(datadescription);

    # Write image capture (Last arg: rescale lookup table), if appropriate.
    coprocessor.WriteImages(datadescription, rescale_lookuptable=rescale_lookuptable,
        image_quality=0, padding_amount=imageFileNamePadding)

    # Live Visualization, if enabled.
    coprocessor.DoLiveVisualization(datadescription, "localhost", 22222)
