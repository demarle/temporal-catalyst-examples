# A sample Catalyst Python pipeline with an ex post facto trigger.
# Here we wait until particles collide and then once they do
# we find out which particles hit and render their paths up to the
# point of collision.

from paraview.simple import *
from paraview import coprocessing

#--------------------------------------------------------------
# Code generated from cpstate.py to create the CoProcessor.


# ----------------------- CoProcessor definition -----------------------

def CreateCoProcessor():
  def _CreatePipeline(coprocessor, datadescription):
    class Pipeline:
      # KEY POINT:
      # We don't have to do anything special at each timestep.
      # But we do have to make the data available.
      # In this case the recent data.
      data = coprocessor.CreateTemporalProducer( datadescription, "volume" )
      points = coprocessor.CreateTemporalProducer( datadescription, "points" )
      #points = coprocessor.CreateProducer( datadescription, "points" )

      # a threshold filter to extract collisions from
      threshold1 = Threshold(Input=data)
      threshold1.Scalars = ['CELLS', 'occupancy']
      threshold1.ThresholdRange = [2.0, 10000.0]      

    return Pipeline()

  class CoProcessor(coprocessing.CoProcessor):
    first = True
    def CreatePipeline(self, datadescription):
      self.Pipeline = _CreatePipeline(self, datadescription)
      # some bookkeeping structures to make a nice set of outputs
      self.StepsWritten = set()
      self.TimesToTimesteps = {}
      self.CCounter = 0
      self.CollisionWeights = {}

    def ProcessTriggers(self, datadescription):
      # In this trigger we Look at the simulation's output, and see if we need to do anything
      # out of the ordinary. We have access to the N most recent outputs so we write them all out to disk.
      data = self.Pipeline.data
      timestep = datadescription.GetTimeStep()
      time = datadescription.GetTime()
      self.TimesToTimesteps[time] = timestep
      data.UpdatePipeline(time)

      points = self.Pipeline.points
      points.UpdatePipeline(time)

      writer = servermanager.writers.XMLPUnstructuredGridWriter(Input=points)
      writer.FileName =  "t_%d.pvtu" % timestep
      #writer.UpdatePipeline(time)
            
      #orange[1] > 1 indicates a collision
      orange = data.GetDataInformation().GetCellDataInformation().GetArrayInformation("occupancy").GetComponentRange(0)

      # must agree in parallel
      from mpi4py import MPI
      comm = MPI.COMM_WORLD
      rank = comm.Get_rank()
      lOccupancy = orange[1]
      gOccupancy = comm.gather(orange[1])
      anyOccupancy = False
      if rank == 0:
        for res in gOccupancy:
          if res > 1:
            anyOccupancy = True
      anyOccupancy = comm.bcast(anyOccupancy)
      #print ("%d %d %d %d" % (rank, timestep, lOccupancy, anyOccupancy))

      if anyOccupancy:
        # a threshold to find out where the collision(s) occured
        threshold1 = FindSource('Threshold1')
        threshold1.UpdatePipeline(time)
        localThreshFilter = threshold1.GetClientSideObject()
        cells = localThreshFilter.GetOutput()
        ncells = cells.GetNumberOfCells()
        centroid = [0,0,0]
        occupancyArray = cells.GetCellData().GetArray("occupancy")

        # a point locator on the input points to find out who was involved
        localPointProducer = points.GetClientSideObject()
        localPoints = localPointProducer.GetOutputDataObject(0)
        idArray = localPoints.GetPointData().GetArray('pointid')
        rArray = localPoints.GetPointData().GetArray('radius')
        NP = localPoints.GetNumberOfPoints()
        import vtk
        pl = vtk.vtkPointLocator()
        pl.SetDataSet(localPoints)
        pl.BuildLocator()
        rl = vtk.vtkIdList()

        # at every cell where a collision happened (of N particles)
        # the N nearest particles were responsible
        newColliders = set()
        for x in range(0, ncells):
          num = occupancyArray.GetValue(x)
          cells.GetCell(x).GetCentroid(centroid)
          #print(centroid, num)
          pl.FindClosestNPoints(int(num), centroid, rl)
          for p in range(0, rl.GetNumberOfIds()):
            #print(rl.GetId(p), idArray.GetValue(rl.GetId(p)), rArray.GetValue(rl.GetId(p)))
            newColliders.add(idArray.GetValue(rl.GetId(p)))

        # must agree in parallel
        #print("L newColliders are: ", sorted(newColliders))
        gColliders = comm.gather(newColliders)
        if rank == 0:
          for hits in gColliders:
            newColliders.update(hits)
        newColliders = comm.bcast(newColliders)
        #print("G newColliders are: ", sorted(newColliders))
        # a collision!
        print ("collision at tstep %d, time %f, maxoccupancy %f" % (timestep, time, orange[1]))
        print ("NEWCOLLIDERS", newColliders)

        self.CollisionWeights[timestep] = {}
        data.UpdatePipelineInformation()
        dtimes = data.TimestepValues
        for hit in newColliders:
          
          for t in dtimes:
            tstep = self.TimesToTimesteps[t]
            if tstep in self.CollisionWeights:
              if hit not in self.CollisionWeights[tstep]:
                self.CollisionWeights[tstep][hit] = 0.5
            else:
              self.CollisionWeights[tstep] = {}
              self.CollisionWeights[tstep][hit] = 0.5
          
          self.CollisionWeights[timestep][hit] = 1.0
      else:
        print ("no collision tstep %d" % timestep)


      #always save the oldest timestep
      data.UpdatePipelineInformation()
      dtimes = data.TimestepValues
      if not dtimes:
        return
      localPointProducer = points.GetClientSideObject()
      localPoints = localPointProducer.GetOutputDataObject(0)
      idArray = localPoints.GetPointData().GetArray('pointid')
      rArray = localPoints.GetPointData().GetArray('radius')
      NP = localPoints.GetNumberOfPoints()

      t = dtimes[0]
      tstep = self.TimesToTimesteps[t]
      if not tstep in self.CollisionWeights:
        weights = {}
      else:
        weights = self.CollisionWeights[tstep]
      print ("at ", tstep, " ", weights)
      # a glyph filter to show all the particles
      glyph1 = Glyph(registrationName='Glyph1', Input=points, GlyphType='Sphere')
      glyph1.UpdatePipeline(time)
      glyph1.ScaleArray = ['POINTS', 'radius']
      glyph1.ScaleFactor = 2.0
      glyph1.GlyphMode = 'All Points'
      glyph1Display = Show(glyph1, GetActiveView(), 'GeometryRepresentation')
      glyph1Display.Representation = 'Surface'
      ColorBy(glyph1Display, ('POINTS', 'pointid'))

      # use color to emphasize the ones involved
      pointidLUT = GetColorTransferFunction('pointid')
      pointidLUT.EnableOpacityMapping = 1
      pointidPWF = GetOpacityTransferFunction('pointid')
      pointidLUT.ScalarRangeInitialized = 1
      pointidPWF.ScalarRangeInitialized = 1
      # a step function turned on for each collider, off for everyone else
      deemph = 0.05
      opLUF = [-1, deemph,deemph,deemph]
      opLUF.extend([-0.75, deemph,deemph,deemph])
      for x in sorted(weights):
        emph = weights[x]
        opLUF.extend([x-0.45, deemph,deemph,deemph])
        opLUF.extend([x-0.25, emph,emph,emph])
        opLUF.extend([x+0.25, emph,emph,emph])
        opLUF.extend([x+0.45, deemph,deemph,deemph])
      opLUF.extend([NP, deemph,deemph,deemph])
      #pointidLUT.RGBPoints = opLUF
      pointidPWF.Points = opLUF

      glyph1.UpdatePipeline(t)
      myView = GetActiveView()
      myView.ViewTime = t;
      if self.first:
        ResetCamera(myView)
        self.first = False
      print ("write ", tstep)
      SaveScreenshot("revent_%d.png"%tstep, myView, ImageResolution=[1024,768])


  coprocessor = CoProcessor()
  freqs = {'volume': [10, 100]}
  coprocessor.SetUpdateFrequencies(freqs)
  return coprocessor

#--------------------------------------------------------------
# Global variables that will hold the pipeline for each timestep
# Creating the CoProcessor object, doesn't actually create the ParaView pipeline.
# It will be automatically setup when coprocessor.UpdateProducers() is called the
# first time.
coprocessor = CreateCoProcessor()

#--------------------------------------------------------------
# Enable Live-Visualizaton with ParaView
coprocessor.EnableLiveVisualization(True, 1)


# ---------------------- Data Selection method ----------------------

def RequestDataDescription(datadescription):
    "Callback to populate the request for current timestep"
    global coprocessor
    if datadescription.GetForceOutput() == True:
        # We are just going to request all fields and meshes from the simulation
        # code/adaptor.
        for i in range(datadescription.GetNumberOfInputDescriptions()):
            datadescription.GetInputDescription(i).AllFieldsOn()
            datadescription.GetInputDescription(i).GenerateMeshOn()
        return

    # setup requests for all inputs based on the requirements of the
    # pipeline.
    coprocessor.LoadRequestedData(datadescription)

# ------------------------ Processing method ------------------------

def DoCoProcessing(datadescription):
    "Callback to do co-processing for current timestep"
    global coprocessor

    # Update the coprocessor by providing it the newly generated simulation data.
    # If the pipeline hasn't been setup yet, this will setup the pipeline.
    coprocessor.UpdateProducers(datadescription)

    # KEY POINT:
    # We make up a trigger here that is evaluated on every timestep.
    coprocessor.ProcessTriggers(datadescription)

    # Write output data, if appropriate.
    coprocessor.WriteData(datadescription);

    # Write image capture (Last arg: rescale lookup table), if appropriate.
    coprocessor.WriteImages(datadescription, rescale_lookuptable=False)

    # Live Visualization, if enabled.
    coprocessor.DoLiveVisualization(datadescription, "localhost", 22222)
